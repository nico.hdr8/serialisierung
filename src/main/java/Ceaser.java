/**
 * Created: 15.03.2023
 *
 * @author Nico Haider (20200527)
 */
public class Ceaser {
    final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public static String decryptString(String value, int key) {
        value = value.toLowerCase();
        StringBuilder decrypted = new StringBuilder();

        for (char c : value.toCharArray()) {
            int index = new StringBuilder(ALPHABET).reverse().toString().indexOf(c);

            if (index != -1) {
                int shift = (index - key + 26) % 26;
                decrypted.append(ALPHABET.charAt(shift));
            } else {
                decrypted.append(c);
            }
        }

        return decrypted.toString();
    }

    public static String encryptString(String value, int key) {
        value = value.toLowerCase();
        StringBuilder encrypted = new StringBuilder();

        for (char c : value.toCharArray()) {
            int index = ALPHABET.indexOf(c);

            if (index != -1) {
                int shift = (index + key) % 26;
                encrypted.append(new StringBuilder(ALPHABET).reverse().toString().charAt(shift));
            } else {
                encrypted.append(c);
            }
        }

        return encrypted.toString().toUpperCase();
    }

    public static String getNumberString(int number) {
        return switch (number) {
            case 0 -> "null";
            case 1 -> "eins";
            case 2 -> "zwei";
            case 3 -> "drei";
            case 4 -> "vier";
            case 5 -> "fuenf";
            case 6 -> "sechs";
            case 7 -> "sieben";
            case 8 -> "acht";
            case 9 -> "neun";
            default -> throw new IllegalArgumentException();
        };
    }

    public static int getStringNumber(String number) {
        return switch (number) {
            case "null" -> 0;
            case "eins" -> 1;
            case "zwei" -> 2;
            case "drei" -> 3;
            case "vier" -> 4;
            case "fuenf" -> 5;
            case "sechs" -> 6;
            case "sieben" -> 7;
            case "acht" -> 8;
            case "neun" -> 9;
            default -> throw new IllegalArgumentException();
        };
    }
}
