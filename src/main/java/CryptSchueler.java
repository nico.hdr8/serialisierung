import java.io.Serializable;

/**
 * Created: 15.03.2023
 *
 * @author Nico Haider (20200527)
 */
public class CryptSchueler implements Serializable {
    String name;
    String vorname;
    String kennung;
    String svn;

    public CryptSchueler(Schueler schueler) {
        this.name = Ceaser.encryptString(schueler.name, 10);
        this.vorname = Ceaser.encryptString(schueler.vorname, 10);

        boolean first = true;
        StringBuilder kennung = new StringBuilder();
        for (char c : String.valueOf(schueler.kennung).toCharArray()) {
            if (first) {
                kennung.append(Ceaser.getNumberString(Character.getNumericValue(c)));
                first = false;
            } else {
                kennung.append(" " + Ceaser.getNumberString(Character.getNumericValue(c)));
            }
        }
        first = true;
        StringBuilder svn = new StringBuilder();
        for (char c : String.valueOf(schueler.svn).toCharArray()) {
            if (first) {
                svn.append(Ceaser.getNumberString(Character.getNumericValue(c)));
                first = false;
            } else {
                svn.append(" " + Ceaser.getNumberString(Character.getNumericValue(c)));
            }
        }
        this.kennung = Ceaser.encryptString(kennung.toString(), 10);
        this.svn = Ceaser.encryptString(svn.toString(), 10);
    }

    public Schueler toSchueler() {
        String name = Ceaser.decryptString(this.name, 10);
        String vorname = Ceaser.decryptString(this.vorname, 10);
        String kennungString = Ceaser.decryptString(this.kennung, 10);
        String svnString = Ceaser.decryptString(this.svn, 10);

        String[] kennungSplit = kennungString.split(" ");
        StringBuilder kennung = new StringBuilder();
        for (String current : kennungSplit) {
            kennung.append(Ceaser.getStringNumber(current));
        }

        String[] svnSplit = svnString.split(" ");
        StringBuilder svn = new StringBuilder();
        for (String current : svnSplit) {
            svn.append(Ceaser.getStringNumber(current));
        }

        return new Schueler(name, vorname, Integer.parseInt(kennung.toString()), Long.parseLong(svn.toString()));
    }
}
