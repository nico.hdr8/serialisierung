import java.io.Serializable;

/**
 * Created: 13.03.2023
 *
 * @author Nico Haider (20200527)
 */
public class Schueler implements Serializable {
    String name;
    String vorname;
    Integer kennung;
    Long svn;

    public Schueler(String name, String vorname, Integer kennung, Long svn) {
        if (kennung < 0) {
            throw new IllegalArgumentException("kennung < 0");
        }
        if ((svn + "").length() != 10) {
            throw new IllegalArgumentException("svn length not 10");
        }
        if (!checkSVN(svn)) {
            throw new IllegalArgumentException("invalid svn");
        }

        this.name = name;
        this.vorname = vorname;
        this.kennung = kennung;
        this.svn = svn;
    }

    private static boolean checkSVN(Long svn) {
        int[] factors = { 3, 7, 9, 5, 8, 4, 2, 1, 6 };

        String svnString = String.valueOf(svn);
        String newSvnString = svnString.substring(0, 3) + svnString.substring(4);

        int sum = 0;
        for (int i = 0; i < factors.length; i++) {
            int factor = factors[i];
            int currentNumber = Character.getNumericValue(newSvnString.charAt(i));

            sum += (currentNumber * factor);
        }

        return (sum % 11) == Character.getNumericValue(svnString.charAt(3));
    }

    @Override
    public String toString() {
        return "Schueler{" +
                "name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", kennung=" + kennung +
                ", svn=" + svn +
                '}';
    }
}
