import java.io.*;

/**
 * Created: 15.03.2023
 *
 * @author Nico Haider (20200527)
 */
public class Application {
    public static void main(String[] args) {
        Schueler[] schueler = {
                new Schueler("Haider", "Nico", 0, 5977110206L),
                new Schueler("Mustermann", "Max", 1, 1237010180L),
                new Schueler("Fischer", "Hans", 2, 1111111111L),
                new Schueler("Bobnev", "Emmanuil", 3, 6010250406L),
                new Schueler("Prinz", "Adrian", 4, 3847150606L),
        };

        CryptSchueler[] cryptSchuelers = new CryptSchueler[schueler.length];
        for (int i = 0; i < cryptSchuelers.length; i++) {
            cryptSchuelers[i] = new CryptSchueler(schueler[i]);
        }

        byte[] bytes = serializeSchueler(cryptSchuelers);
        CryptSchueler[] newSchueler = deserializeSchueler(bytes);
        for (CryptSchueler current : newSchueler) {
            System.out.println(current.toSchueler());
        }
    }

    public static byte[] serializeSchueler(CryptSchueler[] schueler) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(schueler);
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static CryptSchueler[] deserializeSchueler(byte[] bytes) {
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            return (CryptSchueler[]) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
